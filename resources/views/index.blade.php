<!DOCTYPE html>
 <html>
 <head>
   <title></title>
      <link href="css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      .panel{
        margin-bottom: 0px;
      }
      .panel-default {
        border-color: #ddd;
        position: absolute;
        bottom: 0;
        right: 0;
        width: 100%;
    }
    </style>
  
 </head>
 <body>
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">ALTOMATIK INDONESIA</a>
        </div>
        <ul class="nav navbar-nav">
          <li class="active"><a href="#">Home</a></li>
          <li><a href="#">Report</a></li>
          <li><a href="#">about</a></li>
          <li><a href="#"></a></li>
        </ul>
      </div>
    </nav>
    <div class="container">
      <center>
        <table class="margin-height:400px; table-responsive">
          <form name="myForm" id="myForm"  action="import_excel"  class="form-horizontal" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <tr>
              <td> <input type="file" name="import_file" /></td>
            </tr>
            <tr>
              <td> <input type="checkbox" name="drop" value="1" /> <u>Kosongkan tabel sql terlebih dahulu.</u> </td>
            </tr>
            <tr>
              <td> <button type="submit" class="btn btn-primary">Import File</button></td>
            </tr>
          </form>
        </table>
      </center>
    </div>
    <div class="panel panel-default">
      <div class="panel-footer">&copy test</div>
    </div>
 </body>
 </html>