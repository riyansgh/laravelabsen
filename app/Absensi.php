<?php

namespace App;
use App/absensi;
use Illuminate\Database\Eloquent\Model;

class Absensi extends Model
{
    protected $fillable = [
        'date_and_time', 'personnel_id', 'first_name','last_name','card_number','device_name','event_point','verify_type','in_out_status','event_description','remarks');
    ];

}
