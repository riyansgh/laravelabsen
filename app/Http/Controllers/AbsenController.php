<?php

namespace App\Http\Controllers;

use app\Absensi;
use Illuminate\Notifications\Notifiable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use Excel;

class AbsenController extends Controller
{

    public function index()
    {
      return view('testview');
    }

    public function test()
    {
      return "test";
    }

    public function import(Request $request)
    {   
      $drop=$request->drop;
      if($drop == 1){
//             kosongkan tabel absensi
             $bersih = DB::table('absensi')->truncate();
             
      };
      
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader){
          })->get();
            //dd($data);
              if (!empty($data) && $data->count()) {
                foreach ($data as $key => $value) {
                    $insert[] = ['date_and_time' => $value->date_and_time,
                    'personnel_id' => $value->personnel_id,
                    'first_name' => $value->first_name,
                    'last_name' => $value->last_name,
                    'card_number' => $value->card_number,
                    'device_name' => $value->device_name,
                    'event_point' => $value->event_point,
                    'verify_type' => $value->verify_type,
                    'in_out_status' => $value->inout_status,
                    'event_description' => $value->event_description,
                    'remarks' => $value->remarks];
                }
                if(!empty($insert)){
                    DB::table('absensi')->insert($insert);
                    ?><script type="text/javascript"> alert('Import data berhasil') </script>  <?php
                    return view('testview');
                }
              }
      return back();
    }
}
?>